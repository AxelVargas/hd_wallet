require('dotenv').config()
const HDWalletProvider = require('truffle-hdwallet-provider');

//const wallet = new HDWalletProvider(process.env.MNEMONIC,'')
/*
 await web3.eth.getAccounts((err,response)=> console.log(response))
web3.eth.accounts

SimpleStorage.deployed().then((i) => contract = i)

truffle console --network ropsten
truffle migrate --reset --network ropsten
 */
module.exports = {
    networks: {
        ropsten: {
            provider: function(){
                
                return new HDWalletProvider(
                    process.env.MNEMONIC,
                    "https://ropsten.infura.io/v3/"+process.env.INFURA_API_KEY
                )
            },
            //gasPrice: 2500000000,
            network_id: 3,
            // gas: 5000000,
            // confirmation: 2, 
            // timeoutBlocks: 200,
            //skipDryRun: true
            //by default truffle use the first account
        },
        development: {
            host: "127.0.0.1",
            port: 8545,
            network_id: "*"
        }
    },
    
    solc: {
        optimizer: {
            version: "0.4.24",
            enabled: true,
            runs: 200
        }
    }
}